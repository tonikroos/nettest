# nettest

#### 介绍
支持ping，网页打开性能，文件下载性能，网页视频播放性能，dns解析性能测试

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  客户端依赖项
```
1、ffmpeg动态库 ---编译ffmpeg需要，执行client不需要
2、自编译的视频测试软件 videotest 需要libx264.so.157
3、http测试脚本 httptest.sh
4、用于测试网页的 nodejs lighthouse 需要Linux内核大于4.0
5、用于dns测试的 dig
6、用于文件测试的 wget
7、libx264.so.157 更新动态库路径vi /etc/ld.so.conf  写入：/usr/local/lib  后执行:ldconfig 
8、安装google浏览器供lighthouse使用
9、/usr/local/lib64/libstdc++.so.6.0.26
```
2.  安装lighthouse
```
1、安装nodejs

```
3.  xxxx
 
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
