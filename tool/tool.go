package tool

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"os"
	"reflect"
	"runtime"
	"strconv"
	"strings"

	"github.com/rs/zerolog/log"

	"github.com/thedevsaddam/gojsonq/v2"
)

var (
	LogFile string
)

// 校验文件是否存在
func CheckFileIsExist(filename string) bool {
	var exist = true
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		exist = false
	}
	return exist
}

// 去掉空字节
func ByteToString(p []byte) string {
	for i := 0; i < len(p); i++ {
		if p[i] == 0 {
			return string(p[0:i])
		}
	}
	return string(p)
}

// 接口转字符串
func InterfaceToString(interFace interface{}) (string, error) {
	switch interFace.(type) {
	case string:
		return interFace.(string), nil
	case float32:
		return fmt.Sprintf("%.4f", interFace.(float32)), nil
	case float64:
		return fmt.Sprintf("%.8f", interFace.(float64)), nil
	case int:
		return strconv.Itoa(interFace.(int)), nil
	case bool:
		if interFace.(bool) {
			return "true", nil
		}
		return "false", nil
	}
	typeStr := fmt.Sprintf("%v", reflect.TypeOf(interFace))
	return typeStr, errors.New("InterfaceToString undefined type")
}

//整形转换成字节
func IntToBytes(n int) []byte {
	x := int32(n)
	bytesBuffer := bytes.NewBuffer([]byte{})
	binary.Write(bytesBuffer, binary.BigEndian, x)
	return bytesBuffer.Bytes()
}

//字节转换成整形
func BytesToInt(b []byte) int {
	bytesBuffer := bytes.NewBuffer(b)
	var x int32
	binary.Read(bytesBuffer, binary.BigEndian, &x)
	return int(x)
}

func init() {
	switch runtime.GOOS {
	case "darwin":
		panic("system not support")
	case "windows":
		LogFile = ".\\log.txt"
	case "linux":
		LogFile = "./log.txt"
	}
}

func GetJsonFloatToStr(jsonFile string, valPath string) string {
	value, _ := gojsonq.New().File(jsonFile).FindR(valPath)
	valueStr, _ := value.Float64()
	return fmt.Sprintf("%.2f", valueStr)
}

func GetJsonFloat(jsonFile string, valPath string) float64 {
	value, _ := gojsonq.New().File(jsonFile).FindR(valPath)
	valueFloat, _ := value.Float64()
	return valueFloat
}

func CheckTitleExist(jsonMap map[string]interface{}, titles []string) error {

	for _, title := range titles {
		_, ok := jsonMap[title]
		if !ok {
			err := "Json format error,need title:" + title
			log.Error().Msg(err)
			return errors.New(err)
		}
	}

	return nil
}

func PrintStack(err error) {
	if err != nil {
		for i := 1; ; i++ {
			pc, file, line, ok := runtime.Caller(i)
			if !ok {
				break
			}
			f := runtime.FuncForPC(pc)
			if f.Name() != "runtime.main" && f.Name() != "runtime.goexit" {
				//log.Error.Println("file:", file, "line:", line, "funcName:", f.Name(), "error:", err.Error())
				fmt.Println("file:", file, "line:", line, "funcName:", f.Name(), "error:", err.Error())
			}
		}
	}
	return
}

func BandwidthUnitFmt(numStr string, unit string) float64 {
	num, err := strconv.ParseFloat(numStr, 64)
	if err != nil {
		log.Error().Msg(err.Error())
		return 0
	}
	switch strings.ToLower(unit) {
	case "kb/s":
		return float64(num / 1000)
	case "mb/s":
		return float64(num)
	case "gb/s":
		return float64(num * 1000)
	default:
		log.Info().Msg("BandwidthUnitFmt Type not define")
		return 0
	}
}
