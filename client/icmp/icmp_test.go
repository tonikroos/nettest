package icmp

import (
	"fmt"
	"testing"
)

func TestGetIcmpInfo(t *testing.T) {
	url := "baidu.com"
	pingNum := 4
	pingSize := 64
	avgTime, lostRate, ttl, err := GetIcmpInfo(url, pingNum, pingSize)
	fmt.Printf("avgTime:%f; lostRate:%f; ttl:%f\n", avgTime, lostRate, ttl)
	// want := nil                        // 期望的结果
	// if !reflect.DeepEqual(want, got) { // 因为slice不能比较直接，借助反射包中的方法比较
	// 	t.Errorf(want, got) // 测试失败输出错误提示
	// }
	if err != nil { // 因为slice不能比较直接，借助反射包中的方法比较
		t.Errorf("%v:%v:%v;", avgTime, lostRate, ttl) // 测试失败输出错误提示
	}
	t.Log(avgTime, lostRate, ttl)
}
