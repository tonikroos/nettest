package dns

import (
	"fmt"
	"net"
	"time"
)

func GetDNSInfo(src string, TestCount int) (dnsSucRate float64, dnsAvgTime float64) {

	sucessCount := 0
	totalTime := 0.0
	for i := 0; i < TestCount; i++ {
		t1 := time.Now() //多次查询可能会有等待，所以把记时放到循环内
		dst, err := net.LookupIP(src)
		if err != nil {
			continue
		}
		totalTime += float64(time.Since(t1) / 1000000)
		sucessCount = sucessCount + 1
		fmt.Printf("sucessCount:%v; dst:%s ; err:%v\n", sucessCount, dst, err)

	}
	// totalTime := float64(time.Since(t1) / 1000000) //单位转换为ms
	dnsAvgTime = totalTime / float64(sucessCount)
	dnsSucRate = float64(sucessCount / TestCount)
	fmt.Printf("totalTime:%v;  dnsAvgTime:%v; dnsSucRate:%v\n", totalTime, dnsAvgTime, dnsSucRate)
	return
}
