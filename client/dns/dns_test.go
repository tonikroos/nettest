package dns

import "testing"

func TestGetDNSInfo(t *testing.T) {
	src := "google.com"
	dnsSucRate, dnsAvgTime := GetDNSInfo(src, 100)
	if dnsSucRate == 0 {
		t.Errorf("%v:%v;", dnsSucRate, dnsAvgTime)
	}
	t.Logf("%v:%v;", dnsSucRate, dnsAvgTime)
}
