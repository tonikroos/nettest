#！/bin/bash
# 切换yum源

mv "./netmonitor" "/opt"
NetMonitorPath="/opt/netmonitor"
LogFile="/opt/netmonitor/logInstall"
NetMonitorSrvAddr="175.153.165.78:32132"
Update_PATH="/opt/update/"
# 移动文件夹需要在cd前面
# cd /etc/yum.repos.d/
# cp CentOS-Base.repo CentOS-Base-repo.bak
# yum clean all
# centosVer=$(cat /etc/redhat-release|sed -r 's/.* ([0-9]+)\..*/\1/')
# if [ $centosVer == 7 ];then
#     echo "centos version is 7" >>$LogFile
#     wget http://mirrors.aliyun.com/repo/Centos-7.repo
#     mv Centos-7.repo CentOS-Base.repo
# elif [ $centosVer == 8 ];then
#     echo "centos version is 8" >>$LogFile
#     wget http://mirrors.aliyun.com/repo/Centos-8.repo
#     mv Centos-8.repo CentOS-Base.repo
# else
#     echo "centos version $centosVer is not support" >>$LogFile
# fi
# yum makecache
# yum update

yum install bind-utils wget -y 
# 安装chrome
chrome="google-chrome"
if [[ $(whereis $chrome) == "$chrome:" ]];then
    echo "$chrome is not installed.Now begin to install" >>$LogFile
    echo "[google-chrome]
name=google-chrome
baseurl=http://dl.google.com/linux/chrome/rpm/stable/x86_64
enabled=1
gpgcheck=1
gpgkey=https://dl-ssl.google.com/linux/linux_signing_key.pub" > /etc/yum.repos.d/google-chrome.repo
    yum -y install google-chrome-stable --nogpgcheck
    if [[ $(whereis $chrome) != "$chrome:" ]];then
        echo "$chrome is installed sucessfully" >>$LogFile
    else
        echo "$chrome is install failed" >>$LogFile
    fi
else
    echo "$chrome is already installed" >>$LogFile
fi

# 安装Xvfb
yum install Xvfb -y

# 为lighthouse添加用户 
# create user if not exists
user="nettest"
id $user >& /dev/null
if [ $? -ne 0 ];then
    echo "$user not exist! Start creat $user" >>$LogFile
    # 先安装依赖
    yum install perl -y
    password="nettest"
    username="nettest"
    pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
    useradd -m -p $pass $username
    id $username >& /dev/null
    if [ $? -ne 0 ];then
        echo "Create $username failed!" >>$LogFilels
    else
        echo "Create $username Sucess!" >>$LogFile
    fi
else
    echo "$user exist!" >>$LogFile
fi

# 安装nodejs

softname="lighthouse"
out=$(whereis $softname)
if [[ $out == "$softname:" ]];then
    echo "$softname is not installed.Now begin to install" >>$LogFile
    yum install tar -y
    #wget https://nodejs.org/dist/v14.16.0/node-v14.16.0-linux-x64.tar.xz
    tar xvf node-v14.16.0-linux-x64.tar.xz
    mv node-v14.16.0-linux-x64 /opt/nodejs
    ln -s /opt/nodejs/bin/node /usr/bin/node
    ln -s /opt/nodejs/bin/npm /usr/bin/npm
    npm install -g cnpm --registry=https://registry.npm.taobao.org
    ln -s /opt/nodejs/bin/cnpm /usr/bin/cnpm
    cnpm i -g lighthouse
    ln -s /opt/nodejs/bin/lighthouse /usr/bin/lighthouse
    if [[ $(whereis $softname) != "$softname:" ]];then
        echo "$softname is installed sucessfully" >>$LogFile
    else
        echo "$softname is install failed" >>$LogFile
    fi
else
    echo "$softname is already installed" >>$LogFile
fi
# 修改路径
cd $NetMonitorPath
# 修改脚本格式
yum install dos2unix -y
dos2unix httptest.sh upload.sh
# 更改权限
chmod +x httptest.sh client videotest rtty upload.sh
# 创建移动文件
mkdir $Update_PATH
cp upload.sh  $Update_PATH
# 导入ld路径
echo $NetMonitorPath > /etc/ld.so.conf.d/netmonitor.conf
# libcrypto.so.10 -> libcrypto.so.1.0.2k
# libcrypto.so.1.0.2k
# libssl.so.10 -> libssl.so.1.0.2k
# libssl.so.1.0.2k
# libx264.so
# libx264.so.157 -> libx264.so
ldconfig

# 自动启动客户端 
echo "[Unit]
Description=netmonitor

[Service]
Type=simple
Restart=always
RestartSec=5s
ExecStartPre=setenforce 0
ExecStart=$NetMonitorPath/client -serverAddr $NetMonitorSrvAddr
WorkingDirectory=$NetMonitorPath

[Install]
WantedBy=multi-user.target
" > /lib/systemd/system/netmonitor.service
# 启动自定义服务需要关闭selinux  ubuntu 系统需要提前安装apt install selinux-utils
setenforce 0
# 防止重复运行加入重新加载服务配置
systemctl daemon-reload
systemctl start netmonitor.service

sleep 3

# 判断服务状态
ps -aux |awk '{print $11}' | grep "client" >/dev/nullcase 
if [ $? == 0 ];then
    echo "service is started sucessfully" >>$LogFile
else
    echo "service start failed" >>$LogFile
fi
systemctl enable netmonitor
