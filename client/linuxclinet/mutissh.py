import threading
import paramiko

pool = [{'host':'192.168.1.37','username':'root','password':'toni'},
{'host':'192.168.2.34','username':'root','password':'yztx2021'},
]

class paramikoThreading(threading.Thread):
    def __init__(self,command,host,username,password,port=22):
        self.command = command
        self.host = host
        self.username = username
        self.password = password
        self.port = port
 
        super(paramikoThreading,self).__init__()
 
 
    def run(self):
        ssh = paramiko.SSHClient()
        # 创建一个ssh的白名单
        know_host = paramiko.AutoAddPolicy()
        # 加载创建的白名单
        ssh.set_missing_host_key_policy(know_host)
 
        # 连接服务器
        ssh.connect(
            hostname=self.host,
            port=self.port,
            username=self.username,
            password=self.password,
        )
 
        stdin, stdout, stderr = ssh.exec_command(self.command)
        print("*"*60)
        print("ip:%s,\ncommand:%s,\n"%(self.host,self.command))
        print(stdout.read().decode())
        print("*"*60)
        ssh.close()

if __name__ == '__main__':
 
    command = "ls\n  \
touch test\n  \
echo nice >> test\n \
cat test\n \
cd /home\n \
ls\n"
    t_pool = []
    for host in pool:
        t = paramikoThreading(
            host=host.get("host","localhost"),
            username=host.get("username","root"),
            password=host.get("password","toni"),
            command=command
            )
        t_pool.append(t)
    for t in t_pool:
        t.start()
    for t in t_pool:
        t.join()
