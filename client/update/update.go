package update

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"nettest/tool"
	"os"
	"os/exec"
	"path"
	"time"

	"github.com/rs/zerolog/log"
)

const (
	VERSION = "V1.0"
	// CHECK_URL = "http://10.29.48.162:8080/lastestver"
	CHECK_URL = "http://test-dnet-dialtest.app-chengdu1.yunzhicloud.com/baseapi/sys/version"
	// FILE_URL    = "http://10.29.48.162:8080/getfile"
	FILE_URL    = "https://t2.oss-chengdu1.yunzhicloud.com/dnet/netmonitor/netmonitor.tar.gz"
	FILE_PATH   = "/opt/update/"
	FILE_NAME   = FILE_PATH + "netmonitor.tar.gz"
	DestDirPath = "/opt"
	UPLOAD_File = "upload.sh"
	SHELL_File  = "/opt/update/upload.sh"
)

type Reader struct {
	io.Reader
	Total int64 //byte
	Cur   int64
}

func PrtVersion() {
	log.Error().Msgf("version:%s", VERSION)
	fmt.Println("version:", VERSION)
	return
}

func GetFile() (err error) {
	rsp, err := http.Get(FILE_URL)
	if err != nil {
		return
	}
	defer rsp.Body.Close()
	os.MkdirAll(path.Dir(FILE_PATH), os.ModePerm)
	file, err := os.OpenFile(FILE_NAME, os.O_CREATE|os.O_WRONLY, 644)
	if err != nil {
		return
	}
	reader := &Reader{
		Reader: rsp.Body,
		Total:  rsp.ContentLength,
	}

	_, err = io.Copy(file, reader)
	if err != nil {
		return
	}

	return
}

func needUpdate() bool {
	update := false
	resp, err := http.Get(CHECK_URL)
	if err != nil {
		log.Error().Msgf("needUpdate http.Get failed:%s", err)
		return update
	}
	defer resp.Body.Close()
	var httpdata interface{}
	body, err := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(body, &httpdata)
	if err != nil {
		log.Error().Msgf("Unmarshal needUpdate failed：%s", err)
		return update
	}

	dataMap := httpdata.(map[string]interface{})
	titleData := []string{"data"}
	if tool.CheckTitleExist(dataMap, titleData) != nil {
		log.Error().Msg("needUpdate http no data")
		return update
	}
	data := dataMap["data"].(map[string]interface{})

	titleVersion := []string{"version"}
	if tool.CheckTitleExist(data, titleVersion) != nil {
		log.Error().Msg("needUpdate http no version")
		return update
	}

	lastVer := data["version"].(string)

	// lastVer := string(body)
	log.Info().Msgf("lastestVer:%s,currentVer:%v", lastVer, VERSION)
	if lastVer > VERSION {
		update = true
	}
	return update
}

func ExecUpdateSh(shFile string) error {
	cmd := exec.Command("/usr/bin/bash", shFile)
	err := cmd.Run()
	if err != nil {
		log.Error().Msg(err.Error())
		log.Error().Msgf("ExecUpdateSh cmd:%s", cmd)
	}

	return err
}
func CreateUpdateFile() error {
	// dstFile, err := os.Create(SHELL_File)
	// if err != nil {
	// 	fmt.Println(err.Error())
	// 	return
	// }
	dstFile, err := os.OpenFile(SHELL_File, os.O_CREATE|os.O_WRONLY, 744)
	if err != nil {
		log.Error().Msg(err.Error())
		return err
	}
	defer dstFile.Close()
	upload := "#！/bin/bash\n tar -xvf /opt/update/netmonitor.tar.gz -C /opt/ \n systemctl restart netmonitor\n"
	dstFile.WriteString(upload)

	return nil
}
func CheckUpdate() {
	for {
		if !needUpdate() {
			time.Sleep(300 * time.Second)
			continue
		}
		log.Warn().Msgf("CheckUpdate:%v Start updating", VERSION)
		err := GetFile()
		if err != nil {
			log.Error().Msgf("client Get update File failed:%s", err)
			time.Sleep(10 * time.Second)
			continue
		}
		err = CreateUpdateFile()
		if err != nil {
			log.Error().Msgf("CreateUpdateFile failed:%s", err)
			time.Sleep(10 * time.Second)
			continue
		}
		log.Info().Msgf("CreateUpdateFile sucess")
		ExecUpdateSh(SHELL_File)
		time.Sleep(300 * time.Second)
		log.Info().Msgf("client get update sucess!")
	}
}
