package connect

// golang实现带有心跳检测的tcp长连接
// server

import (
	"encoding/json"
	"flag"
	"io"
	"net"
	"nettest/client/task"
	"nettest/client/update"
	"nettest/tool"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
)

const (
	//SERVER_ADDR      string = ":8088"
	Req_REGISTER     string = "REGISTER"
	Req_REGISTER_OK  string = "REGISTER_OK"
	Req_HEARTBEAT    string = "HEARTBEAT"
	Req_DATA         string = "DATA"
	Res_OK           string = "OK"
	DEFAULT_APP_ID   string = "DEFAULT"
	Req_CLIENT_EXIST string = "CLIENT_EXIST"
)

var GLOBAL_APP_ID = DEFAULT_APP_ID
var (
	Dch         chan bool
	Readchan    chan []byte
	Writechan   chan []byte
	APP_ID_FILE string
	sshOn       bool
)

type ReportVerion struct {
	MsgType  int      `json:"msgType"` //0：探针状态报文； 1：任务上报报文； 3：版本号
	ClientId []string `json:"clientId"`
	Version  string   `json:"version"`
}

func init() {
	flag.BoolVar(&sshOn, "ssh", false, "sshOn")
	switch runtime.GOOS {
	case "darwin":
		panic("system not support")
	case "windows":
		APP_ID_FILE = ".\\nettest.txt"
	case "linux":
		APP_ID_FILE = "/usr/local/nettest"
	}
}

func StartSsh(sshAddrIp string) {
	//cmdStr := rtty -I 'dev1' -h '192.168.1.37' -p 5912 -a -v -d '客户端1'
	time.Sleep(3 * time.Second)
	cmdStr := "./rtty -I " + GLOBAL_APP_ID + " -h " + sshAddrIp + " -p 5912 -a -v -d " + "客户端：" + GLOBAL_APP_ID
	cmd := exec.Command("/usr/bin/bash", "-c", cmdStr)
	_, err := cmd.CombinedOutput()
	cmd.Start()
	log.Error().Msgf("StartSsh err:%s", err.Error())
}

func ClientSocket(serverAddr string) {
	log.Info().Msgf("ClientSocket:%s", serverAddr)
	Dch = make(chan bool)
	Readchan = make(chan []byte)
	Writechan = make(chan []byte)
	addr, err := net.ResolveTCPAddr("tcp", serverAddr)
	conn, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		log.Error().Msgf("连接服务端失败:%s", err.Error())
		return
	}
	defer conn.Close()
	go Handler(conn)
	// 是否启用ssh
	sshAddrIp := "127.0.0.1"
	sshAddrArr := strings.Split(serverAddr, ":")
	if sshAddrArr[0] != "" {
		sshAddrIp = sshAddrArr[0]
	}
	log.Info().Msgf("sshOn:%s", sshOn)
	if sshOn {
		log.Error().Msg("client StartSsh")
		go StartSsh(sshAddrIp)
	}
	select {
	case <-Dch:
		log.Error().Msg("与服务器连接中断,正在尝试重连。。。")
		time.Sleep(10 * time.Second)
		// ClientSocket(serverAddr)
		return
	}
}

func ReadAPPIdFromFile(filename string) (string, error) {
	file, err := os.Open(filename)
	if err != nil {
		log.Error().Msg(err.Error())
		return "DEFAULT", err
	}
	//关闭文件
	defer file.Close()
	//Read方法从f中读取最多len(b)字节数据并写入b。它返回读取的字节数和可能遇到的任何错误。文件终止标志是读取0个字节且返回值err为io.EOF
	//定义切片保存读取的数据，要指定容量
	var data []byte = make([]byte, 130)
	_, errR := file.Read(data)
	//出错，同时没有到末尾
	if errR != nil && errR != io.EOF {
		log.Error().Msg(err.Error())
		return "DEFAULT", err
	}

	return tool.ByteToString(data), nil
}

func StoreAPPIdToFile(filename string, appId string) error {
	var err error
	if tool.CheckFileIsExist(filename) { //如果文件存在
		err = os.Remove(filename)
		log.Warn().Msg("Store Remove exsit file")
	}
	file, err := os.Create(filename) //创建文件

	if err != nil {
		return err
	}
	_, err = io.WriteString(file, appId) //写入文件(字符串)
	return err
}

func ReportVersion(conn *net.TCPConn) {
	report := ReportVerion{
		MsgType:  3, // 3：版本更新报文
		ClientId: []string{GLOBAL_APP_ID},
		Version:  update.VERSION,
	}
	reportJson, err := json.Marshal(report)
	if err != nil {
		log.Error().Msgf("ReportVersion json.Marshal:%s", err)
	}
	conn.Write([]byte("DATA|" + string(reportJson)))
}

func Handler(conn *net.TCPConn) {
	// 直到register ok
	dataByte := make([]byte, 128)
	for {
		//GLOBAL_APP_ID = "DEFAULT"
		if tool.CheckFileIsExist(APP_ID_FILE) {
			GLOBAL_APP_ID, _ = ReadAPPIdFromFile(APP_ID_FILE)
		}
		conn.Write([]byte(Req_REGISTER + "|" + GLOBAL_APP_ID))
		conn.Read(dataByte)
		data := tool.ByteToString(dataByte)
		InputsArray := strings.Split(string(data), "|")
		if InputsArray[0] == Req_REGISTER_OK && (len(InputsArray) == 2) {
			log.Info().Msgf("GLOBAL_APP_ID:%s", InputsArray[1])
			if GLOBAL_APP_ID != InputsArray[1] {
				GLOBAL_APP_ID = InputsArray[1]
				err := StoreAPPIdToFile(APP_ID_FILE, GLOBAL_APP_ID)
				if err != nil {
					log.Error().Msg(err.Error())
					Dch <- true
					return
				}
			}
			ReportVersion(conn)
			break
		} else if InputsArray[0] == Req_CLIENT_EXIST {
			log.Warn().Msg("client already opened, connection closed")
			Dch <- true
			return
		} else {
			log.Error().Msg("regist fail")
			Dch <- true
			return
		}
	}
	go ReadHandler(conn)
	go WriteHandler(conn)
	go Work()
}

func ReadHandler(conn *net.TCPConn) {
	log.Debug().Msgf("read")
	const heartbeatTime = 15
	timer1 := time.NewTimer(time.Second * heartbeatTime)
	defer timer1.Stop()
	go func() {
		<-timer1.C
		log.Error().Msgf("heartbeat time expired!")
		Dch <- true
	}()
	for {
		log.Debug().Msgf("read start")
		// 心跳包,回复ack
		data := make([]byte, 4096)
		i, err := conn.Read(data)
		log.Debug().Msgf("ReadHandler:%s,%v,%v", string(data), i, err)
		if err != nil {
			log.Error().Msgf("TCPConn read msg err!")
			Dch <- true
		}

		InputsArray := strings.Split(string(data), "|")
		if InputsArray[0] == Req_HEARTBEAT {
			//conn.Write([]byte(Res_OK + "|"))
			// 同时写可能会导致丢包，此处改为统一由Writechan串行转发
			Writechan <- []byte(Res_OK + "|")
		} else if InputsArray[0] == Req_DATA || InputsArray[0] == "JSON" {
			log.Info().Msgf("recv data:%s", string(data))
			Readchan <- data
		}
		timer1.Reset(heartbeatTime * time.Second) //重新设置定时器
		log.Debug().Msgf("read end")
	}

}

func WriteHandler(conn net.Conn) {
	for {
		select {
		case msg := <-Writechan:
			// 不记录心跳报文
			if len(string(msg)) > 4 {
				log.Info().Msgf("send str :%s", string(msg))
			}
			go conn.Write(msg)
		}
	}

}

func Work() {
	for {
		select {
		case msg := <-Readchan:
			log.Info().Msgf("Work msg:%s", string(msg))
			msgStr := tool.ByteToString(msg)
			jsonIndex := strings.Index(msgStr, `|`)
			if jsonIndex != 4 {
				// data| 或者 json| 第一个|都应该是4, 否则报错
				log.Error().Msgf("Work msg err:%s", msgStr)
				continue
			}
			go task.HandelJsonMsg(msgStr[jsonIndex+1:], Writechan)

		}
	}
}
