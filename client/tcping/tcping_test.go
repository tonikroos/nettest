package tcping

import (
	"fmt"
	"testing"
)

// func TestPing(t *testing.T) {
// 	url := "baidu.com"
// 	port := "443"
// 	duration, remoteAddr, err := Ping(url, port)
// 	fmt.Printf("duration:%v\n", duration)
// 	if err != nil {
// 		t.Errorf("%v:%v;", duration, err)
// 	}
// 	t.Log(duration, remoteAddr, err)
// }

func TestTcping(t *testing.T) {
	url := "baidu.com"
	port := "443"
	// url := "baidu.com"
	// port := "80"
	pingNum := 4
	avgTime, lostRate, ttl, err := Tcping(url, port, pingNum)
	fmt.Printf("avgTime:%f; lostRate:%f; ttl:%f,err:%v\n", avgTime, lostRate, ttl, err)
	if err != nil {
		t.Errorf("%v:%v:%v;", avgTime, lostRate, ttl)
	}
	t.Log(avgTime, lostRate, ttl)
}
