package tcping

import (
	"errors"
	"fmt"
	"math"
	"net"
	"time"

	"github.com/rs/zerolog/log"
)

func timeIt(f func() interface{}) (int64, interface{}) {
	startAt := time.Now()
	err := f()
	endAt := time.Now()
	return endAt.UnixNano() - startAt.UnixNano(), err
}

func Ping(desIp string, port string) (int64, net.Addr, error) {
	var remoteAddr net.Addr
	timeout := 15000
	duration, errIfce := timeIt(func() interface{} {
		conn, err := net.DialTimeout("tcp", fmt.Sprintf("%s:%s", desIp, port), time.Duration(timeout)*time.Millisecond)
		if err != nil {
			log.Error().Msg(err.Error())
			return err
		}
		remoteAddr = conn.RemoteAddr()
		conn.Close()
		return nil
	})
	if errIfce != nil {
		err := errIfce.(error)
		log.Error().Msg(err.Error())
		return 0, remoteAddr, err
	}
	log.Info().Msgf("Tcping Ping duration:%v,remoteAddr:%v", duration, remoteAddr)
	return duration, remoteAddr, nil
}

func Tcping(desIp string, port string, pingNum int) (float64, float64, float64, error) {
	var err error
	log.Info().Msgf("Tcping desIp:%v,port:%v,pingNum:%v", desIp, port, pingNum)
	if pingNum < 1 {
		err = errors.New("Tcping pingNum error")
		log.Error().Msg(err.Error())
		return 0, 0, 0, err
	}
	var FailTimes int // 失败次数
	var SuccessTimes int
	var minTime int64 = int64(math.MaxInt64)
	var maxTime int64
	var totalTime int64
	log.Info().Msgf("Tcping minTime:%v,maxTime:%v,totalTime:%v", minTime, maxTime, totalTime)
	for i := 0; i < pingNum; i++ {
		duration, _, err := Ping(desIp, port)
		if err != nil {
			FailTimes++
			log.Warn().Msgf("tcping failed %v:%v, err:%v,%v", desIp, port, err, time.Millisecond)
			continue
		}
		if minTime > duration {
			minTime = duration
		}
		if maxTime < duration {
			maxTime = duration
		}
		totalTime += duration
		SuccessTimes++
	}

	if minTime == int64(math.MaxInt64) || SuccessTimes == 0 {
		err = errors.New("Tcping result error")
		log.Error().Msg(err.Error())
		return 0, 0, 0, err
	}
	// 上报参数
	lostRate := float64(FailTimes) / float64(SuccessTimes+FailTimes)
	avgTimeF := float64(totalTime) / float64(SuccessTimes)
	avgTime := avgTimeF / 1000000 //转换成ms
	ttl := float64(maxTime-minTime) / (2 * avgTimeF)
	log.Info().Msgf("Tcping lostRate:%v,avgTime:%v,ttl:%v", lostRate, avgTime, ttl)
	return avgTime, lostRate, ttl, nil
}
