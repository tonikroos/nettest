package task

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"nettest/client/dns"
	"nettest/client/download"
	"nettest/client/icmp"
	"nettest/client/tcping"
	"strconv"

	"github.com/rs/zerolog/log"

	otto "github.com/dop251/goja"
)

const ()

type TaskHandle func(StructTask, map[string]interface{}, *interface{}) error

/*
type Foo struct {
}
type Bar struct {
}
regStruct = make(map[string]interface{})
 regStruct["baidu.com"] = ReportPing{}
 regStruct["Bar"] = Bar{}

 report := ReportBase{
 clientId:1353579454437265408,
 taskId:1
 }
*/
type ReportBase struct {
	MsgType  int         `json:"msgType"` //0：探针状态报文 1：任务上报报文
	ClientId []string    `json:"clientId"`
	TaskId   string      `json:"taskId"`
	ExecId   string      `json:"execId"`
	TaskType string      `json:"taskType"`
	Url      interface{} `json:"urlInfo"`
}

type ReportPing struct {
	ReportDetil
	AvgTime string `json:"avgTime"`
	Loss    string `json:"loss"`
	Ttl     string `json:"ttl"`
}

type ReportDns struct {
	ReportDetil
	DnsSucRate float64 `json:"dnsRate"`
	DnsAvgTime float64 `json:"dnsTime"`
}

type ReportDownload struct {
	ReportDetil
	Bandwidth    string `json:"bandwidth"`
	Size         string `json:"size"`
	DownloadTime string `json:"time"`
}

type ReportHttp struct {
	ReportDetil
	Performance     float64 `json:"performance"`
	Accessibility   float64 `json:"accessibility"`
	Practices       float64 `json:"practices"`
	Seo             float64 `json:"seo"`
	FirstTime       float64 `json:"firstTime"`
	LastTime        float64 `json:"lastTime"`
	InteractiveTime float64 `json:"interactiveTime"`
	BlockTime       float64 `json:"blockTime"`
}

type ReportVideo struct {
	ReportDetil
	BlockFreq   float64 `json:"blockFreq"`
	BlockTime   float64 `json:"blockTime"`
	ConnectTime float64 `json:"connectTime"`
}

type ReportDetil struct {
	Status   string `json:"status"`
	Url      string `json:"url"`
	TargetId string `json:"targetId"`
}

var TaskHandleMap map[string]TaskHandle

func init() {
	TaskHandleMap = make(map[string]TaskHandle)
	TaskHandleMap["ping"] = TaskHandlePing
	TaskHandleMap["http"] = TaskHandleHttp
	TaskHandleMap["download"] = TaskHandleDownload
	TaskHandleMap["dns"] = TaskHandleDns
	TaskHandleMap["video"] = TaskHandleVideo
	TaskHandleMap["game"] = TaskHandleGame
}

func (p *StructTask) GetPing() error {
	return nil
}

func (p *StructTask) GetHttp() error {

	filePath := "/home/test/js/nodetest/index.js"
	//先读入文件内容
	bytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		//fmt.Println("GetHttp ReadFile err:", err)
	}

	vm := otto.New()
	_, err = vm.RunString(string(bytes))
	if err != nil {
		//fmt.Println("GetHttp vm.Run err:", err)
	}

	return nil
}

func GetIcmpInfo(url string, pingNum string, pingSize string, reportStruct *ReportPing) error {
	fmt.Printf("url:%v; pingNum:%v; pingSize:%v\n", url, pingNum, pingSize)
	num, err := strconv.Atoi(pingNum)
	if err != nil {
		return err
	}
	size, err := strconv.Atoi(pingSize)
	if err != nil {
		return err
	}
	avgTime, lostRate, ttl, err := icmp.GetIcmpInfo(url, num, size)
	fmt.Printf("avgTime:%f; lostRate:%f; ttl:%f\n", avgTime, lostRate, ttl)
	if err != nil {
		return err
	}
	reportStruct.AvgTime = strconv.FormatFloat(avgTime, 'f', 2, 64)
	reportStruct.Loss = strconv.FormatFloat(lostRate, 'f', 2, 64)
	reportStruct.Ttl = strconv.FormatFloat(ttl, 'f', 2, 64)
	return err
}

func TaskHandlePing(taskInst StructTask, sendMap map[string]interface{}, reportUrlInfo *interface{}) error {
	urls := sendMap["urlInfo"].([]interface{})
	reportArr := []ReportPing{}
	for _, urlIntf := range urls {
		url := urlIntf.(map[string]interface{})
		var reportStruct ReportPing
		reportStruct.Url = url["url"].(string)
		reportStruct.TargetId = fmt.Sprintf("%v", url["targetId"])
		pingCount := fmt.Sprintf("%v", url["pingCount"])
		pingSize := fmt.Sprintf("%v", url["pingSize"])
		//taskInst.GetPing()
		//err := taskInst.GetPingInfo(url["url"].(string), pingCount, pingSize, &reportStruct)
		err := GetIcmpInfo(url["url"].(string), pingCount, pingSize, &reportStruct)
		if err != nil {
			reportStruct.Status = err.Error()
		}
		reportArr = append(reportArr, reportStruct)
	}
	*reportUrlInfo = reportArr
	return nil
}

func TaskHandleHttp(taskInst StructTask, sendMap map[string]interface{}, reportUrlInfo *interface{}) error {
	urls := sendMap["urlInfo"].([]interface{})
	reportArr := []ReportHttp{}
	for _, urlIntf := range urls {
		url := urlIntf.(map[string]interface{})
		var reportStruct ReportHttp
		reportStruct.Url = url["url"].(string)
		reportStruct.TargetId = fmt.Sprintf("%v", url["targetId"])
		//err := taskInst.GetHttp()
		err := taskInst.GetHttpInfo(url["url"].(string), &reportStruct)
		if err != nil {
			reportStruct.Status = err.Error()
		}
		reportArr = append(reportArr, reportStruct)
	}
	*reportUrlInfo = reportArr
	return nil
}

func TaskHandleDownload(taskInst StructTask, sendMap map[string]interface{}, reportUrlInfo *interface{}) error {
	//"https://gitee.com/tonikroos/nettest/attach_files/619834/download/release.zip"
	taskId := fmt.Sprintf("%v", sendMap["taskId"])
	execId := fmt.Sprintf("%v", sendMap["execId"])
	urls := sendMap["urlInfo"].([]interface{})
	reportArr := []ReportDownload{}
	for _, urlIntf := range urls {
		url := urlIntf.(map[string]interface{})
		var reportStruct ReportDownload
		reportStruct.Url = url["url"].(string)
		reportStruct.TargetId = fmt.Sprintf("%v", url["targetId"])
		// err := taskInst.GetDownlaodInfo(url["url"].(string), (taskId + execId), &reportStruct)
		// if err != nil {
		// 	reportStruct.Status = err.Error()
		// }
		// bwStr := reportStruct.Bandwidth
		// bwArr := strings.Split(bwStr, " ")
		// bwFloat := tool.BandwidthUnitFmt(bwArr[0], bwArr[1]) // 单位转换为MB/s
		Bandwidth, Size, DownloadTime, err := download.GetDownlaodInfo(url["url"].(string), (taskId + execId))
		if err != nil {
			return err
		}
		// 上报参数
		reportStruct.Bandwidth = Bandwidth
		reportStruct.Size = Size
		reportStruct.DownloadTime = DownloadTime
		log.Info().Msgf("Bandwidth:%v", Bandwidth)
		reportArr = append(reportArr, reportStruct)
	}
	*reportUrlInfo = reportArr
	return nil
}

func TaskHandleDns(taskInst StructTask, sendMap map[string]interface{}, reportUrlInfo *interface{}) error {
	// dnsType := fmt.Sprintf("%v", sendMap["dnsType"])
	// dnsIpv4 := fmt.Sprintf("%v", sendMap["dnsIpv4"])
	urls := sendMap["urlInfo"].([]interface{})
	reportArr := []ReportDns{}
	for _, urlIntf := range urls {
		url := urlIntf.(map[string]interface{})
		var reportStruct ReportDns
		reportStruct.Url = url["url"].(string)
		reportStruct.TargetId = fmt.Sprintf("%v", url["targetId"])
		TestCount := 100 // 默认测试100次dns
		// sucessCount := 0
		// totalTime := 0.0
		// var dnserr error
		// for i := 0; i < TestCount; i++ {
		// 	dnserr, dnsStatus, dnsTime := taskInst.GetDNSInfo(url["url"].(string))
		// 	if dnserr != nil {
		// 		log.Error().Msgf("dnserr break:%s", dnserr)
		// 		break
		// 	}
		// 	// 注意空格
		// 	if dnsStatus != " NOERROR" {
		// 		continue
		// 	}
		// 	sucessCount = sucessCount + 1
		// 	totalTime = totalTime + dnsTime
		// }

		// if dnserr != nil {
		// 	log.Error().Msg("dnstesterr" + dnserr.Error())
		// 	reportStruct.Status = dnserr.Error()
		// 	continue
		// }
		dnsSucRate, dnsAvgTime := dns.GetDNSInfo(url["url"].(string), TestCount)
		reportStruct.DnsSucRate = dnsSucRate
		reportStruct.DnsAvgTime = dnsAvgTime
		reportArr = append(reportArr, reportStruct)
	}
	*reportUrlInfo = reportArr
	return nil
}

func TaskHandleVideo(taskInst StructTask, sendMap map[string]interface{}, reportUrlInfo *interface{}) error {

	urls := sendMap["urlInfo"].([]interface{})
	reportArr := []ReportVideo{}
	for _, urlIntf := range urls {
		url := urlIntf.(map[string]interface{})
		var reportStruct ReportVideo
		reportStruct.Url = url["url"].(string)
		reportStruct.TargetId = fmt.Sprintf("%v", url["targetId"])
		tesTime := fmt.Sprintf("%v", url["tesTime"])
		err := taskInst.GetVideoTestInfo(url["url"].(string), tesTime, &reportStruct)
		if err != nil {
			reportStruct.Status = err.Error()
		}
		reportArr = append(reportArr, reportStruct)
	}
	*reportUrlInfo = reportArr
	return nil
}

func TaskHandleGame(taskInst StructTask, sendMap map[string]interface{}, reportUrlInfo *interface{}) error {
	urls := sendMap["urlInfo"].([]interface{})
	reportArr := []ReportPing{}
	for _, urlIntf := range urls {
		url := urlIntf.(map[string]interface{})
		var reportStruct ReportPing
		urlStr := url["url"].(string)
		reportStruct.Url = urlStr
		reportStruct.TargetId = fmt.Sprintf("%v", url["targetId"])
		pingPort := fmt.Sprintf("%v", url["pingPort"])
		pingCount := fmt.Sprintf("%v", url["pingCount"])
		conut, _ := strconv.Atoi(pingCount)

		avgTime, lostRate, ttl, err := tcping.Tcping(urlStr, pingPort, conut)
		if err != nil {
			reportStruct.Status = err.Error()
		}
		reportStruct.AvgTime = strconv.FormatFloat(avgTime, 'f', 2, 64)
		reportStruct.Loss = strconv.FormatFloat(lostRate, 'f', 2, 64)
		reportStruct.Ttl = strconv.FormatFloat(ttl, 'f', 2, 64)

		reportArr = append(reportArr, reportStruct)
	}
	*reportUrlInfo = reportArr
	return nil
}

func HandelJsonMsg(msg string, reportChan chan []byte) {
	// 使用json格式解析
	var i interface{}
	err := json.Unmarshal([]byte(msg), &i)
	if err != nil {
		log.Error().Msgf("Unmarshal HandelJsonMsg failed：%s", err)
		return
	}
	//默认转成了map
	//4.解析到interface的json可以判断类型
	dataMap := i.(map[string]interface{})
	taskType := fmt.Sprintf("%v", dataMap["taskType"])
	taskId := fmt.Sprintf("%v", dataMap["taskId"])
	execId := fmt.Sprintf("%v", dataMap["execId"])
	clientId := fmt.Sprintf("%v", dataMap["clientId"])
	log.Info().Msgf("HandelJsonMsg:%s", dataMap)

	// 上报结构体
	var urlStruct interface{}
	report := ReportBase{
		MsgType:  1, // 1：任务上报报文
		ClientId: []string{clientId},
		TaskId:   taskId,
		ExecId:   execId,
		TaskType: taskType,
		Url:      &urlStruct,
	}

	// 发送上报数据
	handleFunc, ok := TaskHandleMap[taskType]
	if !ok {
		log.Error().Msgf("task type not defined：%s", taskType)
		return
	}
	taskInst := StructTask{}
	err = handleFunc(taskInst, dataMap, &urlStruct)
	if err != nil {
		log.Error().Msgf("taskFailed：%s", err)
	}
	reportJson, err := json.Marshal(report)
	if err != nil {
		log.Error().Msgf("json.Marshal:%s", err)
	}
	log.Info().Msgf("reportJson:%s", string(reportJson))
	reportChan <- []byte("DATA|" + string(reportJson))

}
