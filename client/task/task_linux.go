package task

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"nettest/tool"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog/log"

	"github.com/thedevsaddam/gojsonq/v2"
)

const (
	PING_AVG_POS          = 4 // rtt min/avg/max/mdev = 0.193/0.361/0.560/0.166 ms
	PING_TTL_POS          = 6 // rtt min/avg/max/mdev = 0.193/0.361/0.560/0.166 ms
	PING_PKT_LOSS         = 2 // 4 packets transmitted, 4 received, 0% packet loss, time 3003ms
	DNS_QUERY_TIME_LINE   = 6 // ;; Query time: 77 msec
	DNS_QUERY_TIME        = 3 // ;; Query time: 77 msec
	DNS_QUERY_STATUS_LINE = 4 // ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 7464
	DNS_QUERY_STATUS      = 2 //  NOERROR, id: 7464
	DOWNLOAD_LINE         = 3 // 2020-11-17 09:40:18 (218 KB/s) - ‘./temp/htmlquery.git’ saved [162057]
	DOWNLOAD_TIME         = 5 //    [   <=>  ] 1,641,989   2.93MB/s   in 0.5s
)

type StructTask struct{}

func GetExecCommondInfoNOSplit(cmdStr string) (string, error) {
	cmd := exec.Command("/usr/bin/bash", "-c", cmdStr)
	out, err := cmd.CombinedOutput()
	cmd.Start()
	log.Info().Msgf("Linux cmd:%s", cmd)
	log.Trace().Msgf("out:%s", out)
	if err != nil {
		log.Error().Msg(err.Error())
		return " ", err
	}
	return string(out), nil
}

func GetExecCommondInfo(cmdStr string) ([]string, error) {
	allStr, err := GetExecCommondInfoNOSplit(cmdStr)
	if err != nil {
		return nil, err
	}
	lines := strings.Split(allStr, "\n")

	return lines, nil
}

func (p *StructTask) GetPingInfo(url string, pingNum string, pingSize string, reportStruct *ReportPing) error {
	// Ping Url -c packetCount -s pingSize
	execCmd := "ping -c " + pingNum + " " + url + " -s " + pingSize + " -i 0.2"

	pingInfo, err := GetExecCommondInfo(execCmd)
	if err != nil {
		return err
	}
	pktLossLine := pingInfo[len(pingInfo)-3]
	reptLine := pingInfo[len(pingInfo)-2]
	// 获取时延
	reportInfo := strings.Split(reptLine, "/")
	if len(reportInfo) <= PING_AVG_POS {
		err = errors.New("reportInfo len error")
		return err
	}
	//avg, err := strconv.ParseFloat(reportInfo[PING_AVG_POS], 32)
	// 获取丢包率
	pktLossInfo := strings.Split(pktLossLine, ",")
	if len(reportInfo) <= PING_PKT_LOSS {
		err = errors.New("pktLossInfo len error")
		return err
	}
	lossInfo := strings.Split(pktLossInfo[PING_PKT_LOSS], "%") // 0% packet loss
	pktLossStr := strings.Replace(lossInfo[0], " ", "", -1)
	// 获取抖动
	ttlInfo := strings.Split(reportInfo[PING_TTL_POS], " ")

	// 上报参数

	reportStruct.AvgTime = reportInfo[PING_AVG_POS]
	reportStruct.Loss = pktLossStr
	reportStruct.Ttl = ttlInfo[0]

	return nil
}

func (p *StructTask) GetDNSInfo(dnsUrl string) (error, string, float64) {
	// dig baidu.com any 获取时间；不加any需要不加本地网关缓存
	execCmd := "dig " + dnsUrl
	dnsInfo, err := GetExecCommondInfo(execCmd)
	if err != nil {
		log.Error().Msg(err.Error())
		return err, "", 0
	}
	// 防止dns服务器阻止，至少20s超时；
	//time.Sleep(20 * time.Second)
	// status
	if len(dnsInfo) < DNS_QUERY_STATUS_LINE {
		return errors.New("dns len err!"), "", 0
	}

	statusLine := dnsInfo[DNS_QUERY_STATUS_LINE]
	statusArr := strings.Split(statusLine, ":")
	if len(statusLine) < DNS_QUERY_STATUS {
		return errors.New("dns status len err!"), "", 0
	}
	status := strings.Split(statusArr[DNS_QUERY_STATUS], ",")
	statusStr := status[0]

	// dnstime
	queryTimeLine := dnsInfo[len(dnsInfo)-DNS_QUERY_TIME_LINE]
	queryTime := strings.Split(queryTimeLine, " ")
	dnstime, err := strconv.ParseFloat(queryTime[DNS_QUERY_TIME], 64)
	if err != nil {
		log.Error().Msg(err.Error())
	}
	return nil, statusStr, dnstime
}

func (p *StructTask) GetDownlaodInfo(downloadUrl string, taskId string, reportStruct *ReportDownload) error {
	// wget baidu.com -P ./temp
	// wget baidu.com -P ./temp -d
	execCmd := "wget " + downloadUrl + " -P ./" + taskId
	downloadInfo, err := GetExecCommondInfo(execCmd)
	if err != nil {
		return err
	}
	// 删除文件夹
	execCmd = "rm -rf ./" + taskId
	_, _ = GetExecCommondInfo(execCmd)

	downloadInfoLine := downloadInfo[len(downloadInfo)-DOWNLOAD_LINE]
	// 带宽
	bwIndex1 := strings.Index(downloadInfoLine, `(`)
	bwIndex2 := bwIndex1
	for string(downloadInfoLine[bwIndex2]) != ")" {
		bwIndex2++
	}
	bandwidth := downloadInfoLine[bwIndex1+1 : bwIndex2]
	// 总大小
	sizeIndex1 := strings.Index(downloadInfoLine, `[`)
	sizeIndex2 := sizeIndex1
	for string(downloadInfoLine[sizeIndex2]) != "]" {
		sizeIndex2++
	}
	sizeStr := downloadInfoLine[sizeIndex1+1 : sizeIndex2]
	size := strings.Split(sizeStr, "/") // byte
	// 下载时间
	timeLine := downloadInfo[len(downloadInfo)-DOWNLOAD_TIME]
	timeStr := strings.Split(timeLine, " ")
	time := timeStr[len(timeStr)-1] // 6.98M=0s
	timeSec := strings.Split(time, "=")
	realTime := timeSec[len(timeSec)-1]
	// 上报参数
	reportStruct.Bandwidth = bandwidth
	reportStruct.Size = size[0]
	reportStruct.DownloadTime = realTime
	return nil
}

func Get(url string) string {

	// 超时时间：5秒
	client := &http.Client{Timeout: 5 * time.Second}
	resp, err := client.Get(url)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	var buffer [512]byte
	result := bytes.NewBuffer(nil)
	for {
		n, err := resp.Body.Read(buffer[0:])
		result.Write(buffer[0:n])
		if err != nil && err == io.EOF {
			break
		} else if err != nil {
			panic(err)
		}
	}

	return result.String()
}

func IsProcessExist(proName string) bool {
	// RsyncProcess_NUM=`ps aux | grep startRsync.sh | grep -v grep | awk '{print $2}' | wc -l`
	execCmd := "ps aux | grep '" + proName + "' | grep -v grep | awk '{print $2}' | wc -l"
	proNum, _ := GetExecCommondInfoNOSplit(execCmd)
	if proNum == "1\n" {
		return true
	}
	return false
}

func (p *StructTask) GetDisplayID() int {
	displayID := 1
	for i := 1; i < 100; i++ {
		displayName := "Xvfb -ac :" + strconv.Itoa(i)
		if !IsProcessExist(displayName) {
			displayID = i
			break
		}
	}
	return displayID
}

func ExecHttpTest(shFile string, testUser string, displayID string, testUrl string) error {
	cmd := exec.Command("/usr/bin/bash", shFile, testUser, displayID, testUrl)
	err := cmd.Run()
	if err != nil {
		log.Error().Msg(err.Error())
		log.Error().Msgf("cmd：%s", cmd)
	}

	return err
}

func GetJsonFloatToStr(jsonFile string, valPath string) string {
	value, _ := gojsonq.New().File(jsonFile).FindR(valPath)
	valueStr, _ := value.Float64()
	return fmt.Sprintf("%.2f", valueStr)
}

func (p *StructTask) GetHttpInfo(url string, reportStruct *ReportHttp) error {
	// 设置xvfb窗口服务器  Xvfb :1 -ac & 每个进程分配一个，防止误kill   Xvfb -ac :99
	displayID := strconv.Itoa(p.GetDisplayID())
	displayName := "Xvfb -ac :" + displayID
	execCmd := displayName + " >/dev/null 2>&1 &"
	go GetExecCommondInfoNOSplit(execCmd)
	// 关闭xvfb窗口服务器 defer ： ps -ef|grep "Xvfb :9 -ac"|grep -v grep|cut -c 9-16|xargs kill -9
	closeDisplay := "ps -ef|grep '" + displayName + "'|grep -v grep|cut -c 9-16|xargs kill -9"
	defer GetExecCommondInfoNOSplit(closeDisplay)
	// time.Sleep(2)
	shFile := "./httptest.sh"
	testUser := "nettest"
	err := ExecHttpTest(shFile, testUser, displayID, url)
	if err != nil {
		return err
	}
	//生成测试报告 lighthouse https://www.baidu.com/ --output=json --output-path=./baidu.json --save-assets=
	outPutFile := "/home/nettest/" + displayID + ".json"

	performance := tool.GetJsonFloat(outPutFile, "categories.performance.score")
	accessibility := tool.GetJsonFloat(outPutFile, "categories.accessibility.score")
	practices := tool.GetJsonFloat(outPutFile, "categories.best-practices.score")
	seo := tool.GetJsonFloat(outPutFile, "categories.seo.score")

	// 第一个渲染完成时间
	firstTime := tool.GetJsonFloat(outPutFile, "audits.first-contentful-paint.numericValue")
	// 最后一个渲染完成时间
	lastTime := tool.GetJsonFloat(outPutFile, "audits.largest-contentful-paint.numericValue")
	// 总交互时间
	interactiveTime := tool.GetJsonFloat(outPutFile, "audits.interactive.numericValue")
	// 总阻塞时间
	blockTime := tool.GetJsonFloat(outPutFile, "audits.total-blocking-time.numericValue")
	// 删除临时文件
	rmFile := "rm -rf /home/nettest/" + displayID + "*"
	defer GetExecCommondInfoNOSplit(rmFile)

	// 上报参数
	reportStruct.Performance = performance
	reportStruct.Accessibility = accessibility
	reportStruct.Practices = practices
	reportStruct.Seo = seo
	reportStruct.FirstTime, _ = strconv.ParseFloat(fmt.Sprintf("%.2f", firstTime), 64)
	reportStruct.LastTime, _ = strconv.ParseFloat(fmt.Sprintf("%.2f", lastTime), 64)
	reportStruct.InteractiveTime, _ = strconv.ParseFloat(fmt.Sprintf("%.2f", interactiveTime), 64)
	reportStruct.BlockTime, _ = strconv.ParseFloat(fmt.Sprintf("%.2f", blockTime), 64)
	return nil
}

func (p *StructTask) GetVideoTestInfo(videoUrl string, testTime string, reportStruct *ReportVideo) error {
	// videotest  -u http://10.29.48.162:8080/ -T 10
	execCmd := "./videotest -u " + videoUrl + " -T " + testTime

	outputStr, err := GetExecCommondInfoNOSplit(execCmd)
	if err != nil {
		return err
	}
	bwIndex1 := strings.Index(outputStr, `{`)
	bwIndex2 := bwIndex1
	for string(outputStr[bwIndex2]) != "}" {
		bwIndex2++
	}
	videoTestStr := outputStr[bwIndex1 : bwIndex2+1]
	//2.声明空interface
	var i interface{}
	//3.解析
	err = json.Unmarshal([]byte(videoTestStr), &i)
	if err != nil {
		log.Error().Msg(err.Error())
		return err
	}
	//默认转成了map
	//4.解析到interface的json可以判断类型
	testMap := i.(map[string]interface{})

	block_cnt := testMap["block_cnt"].(float64)
	block_time := testMap["block_time"].(float64)   // 单位ms
	play_time := testMap["play_time"].(float64)     // 单位s
	connectTime := testMap["connectTime"].(float64) // 单位ms

	// 上报参数
	// 阻塞频率为每600s的阻塞次数
	reportStruct.BlockFreq = (block_cnt / play_time) * 600
	reportStruct.BlockTime = block_time
	reportStruct.ConnectTime = connectTime
	return nil
}
