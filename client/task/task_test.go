package task

import (
	"fmt"
	"testing"
)

func Benchmark_HandelJsonMsg(b *testing.B) {
	fmt.Println("Benchmark_HandelJsonMsg")
	b.StopTimer() // 调用该函数停止压力测试的时间计数

	// 做一些初始化，例如读取文件数据，数据库连接之类的
	// 这样这些时间不影响我们测试函数本身的性能
	jsonByte := []byte(`{
		"clientId":"1353579454437265408",
		"taskId":1,
		"execId":26,
		"taskType":"ping",
		"url":[
		"baidu.com",
		"175.153.174.137",
		"192.168.1.40"
		],
		"pingCount":4,
		"pingSize":64
	}`)

	Writechan := make(chan []byte)

	b.N = 1000     // 可以修改执行次数
	b.StartTimer() // 重新开始时间计时
	for i := 0; i < b.N; i++ {
		fmt.Println("Benchmark_HandelJsonMsgStart:", i)
		go HandelJsonMsg(string(jsonByte), Writechan)
	}
}

func TestGetIcmpInfo(t *testing.T) {
	url := "baidu.com"
	pingNum := "4"
	pingSize := "64"
	var reportStruct ReportPing
	err := GetIcmpInfo(url, pingNum, pingSize, &reportStruct)
	if err != nil {
		t.Errorf("%v", err)
	}
	fmt.Printf("avgTime:%v; lostRate:%v; ttl:%v\n", reportStruct.AvgTime, reportStruct.Loss, reportStruct.Ttl)
	// t.Log(avgTime, lostRate, ttl)
}
