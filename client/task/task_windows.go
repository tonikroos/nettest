package task

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"nettest/tool"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog/log"

	"github.com/axgle/mahonia" // 解析window gbk 字符
	// 解析json文件
)

const (
	PING_LEN              = 16
	PING_AVG_TIME         = 15
	PING_PKT_LOSS         = 12
	DNS_QUERY_TIME_LINE   = 6 // ;; Query time: 77 msec
	DNS_QUERY_TIME        = 3 // ;; Query time: 77 msec
	DNS_QUERY_STATUS_LINE = 4 // ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 7464
	DNS_QUERY_STATUS      = 2 //  NOERROR, id: 7464
	DOWNLOAD_LINE         = 2 // 2020-11-17 09:40:18 (218 KB/s) - ‘./temp/htmlquery.git’ saved [162057]
	DOWNLOAD_TIME_LINE    = 4 //   0K ..     100% 28.3M=0s
)

type StructTask struct{}

func (p *StructTask) getPingCommondInfo(cmdStr string) ([]string, error) {
	str, err := p.getExeCmdInfo(cmdStr)
	if err != nil {
		log.Error().Msg(err.Error())
		return nil, err
	}

	lines := strings.Split(str, "=")
	return lines, nil
}

func (p *StructTask) getExeCmdInfo(cmdStr string) (string, error) {
	result, err := exec.Command("cmd", "/c", cmdStr).Output()
	log.Info().Msgf("Windows Cmd:%s", cmdStr)
	if err != nil {
		log.Error().Msg(err.Error())
		return " ", err
	}
	enc := mahonia.NewDecoder("gbk")
	str := enc.ConvertString(string(result))
	return str, nil
}
func splitPingTime(timeStr string) string {
	timeLines := strings.Split(timeStr, "m")
	return strings.Replace(timeLines[0], " ", "", -1)
}

func getTtlTime(maxTime string, minTime string) string {
	maxTimeFloat, err := strconv.ParseFloat(maxTime, 32)
	if err != nil {
		return "calcErr"
	}
	minTimeFloat, err := strconv.ParseFloat(minTime, 32)
	if err != nil {
		return "calcErr"
	}
	ttl := maxTimeFloat - minTimeFloat
	return fmt.Sprintf("%.2f", ttl)
}

func (p *StructTask) GetPingInfo(pingIp string, pingNum string, pingSize string, reportStruct *ReportPing) error {
	//println("call windows GetPingInfo", pingIp, pingNum, pingSize)
	execCmd := "ping -n " + pingNum + " " + pingIp + " -l " + pingSize
	println("exec  windows ping:", execCmd)

	pingInfo, err := p.getPingCommondInfo(execCmd)
	if err != nil {
		return err
	}
	pingLen := len(pingInfo)
	if pingLen < 5 {
		err = errors.New("pingInfo len error")
		return err
	}
	// 平均时延
	avgTime := splitPingTime(pingInfo[pingLen-1])
	// 抖动
	maxTime := splitPingTime(pingInfo[pingLen-2])
	minTime := splitPingTime(pingInfo[pingLen-3])
	ttlTime := getTtlTime(maxTime, minTime)

	// 丢包率
	pktLossStr := pingInfo[pingLen-4]
	pktLossLines := strings.Split(pktLossStr, "(")
	pktLossLine := strings.Replace(pktLossLines[0], " ", "", -1)

	// 上报参数
	reportStruct.AvgTime = avgTime
	reportStruct.Loss = pktLossLine
	reportStruct.Ttl = ttlTime
	return nil
}

func getTempPath(pathTmp string) string {
	if tool.CheckFileIsExist(pathTmp) {
		pathTmp = pathTmp + "1"
		getTempPath(pathTmp)
	}
	return pathTmp
}

func (p *StructTask) GetHttpInfo(url string, reportStruct *ReportHttp) error {
	// 新建临时文件夹
	pathTmp := getTempPath(".\\temphttp")
	log.Info().Msgf("pathTmp:%s", pathTmp)
	err := os.Mkdir(pathTmp, 0666)
	if err != nil {
		log.Error().Msg(err.Error())
		return err
	}
	defer os.RemoveAll(pathTmp)
	outPutFile := pathTmp + "\\report.json"
	// 生成测试报告
	execCmd := "lighthouse " + url + " --output=json --output-path=" + outPutFile + " --save-assets"
	_, err = exec.Command("cmd", "/c", execCmd).Output()
	if err != nil {
		log.Error().Msg(err.Error())
		return err
	}
	performance := tool.GetJsonFloat(outPutFile, "categories.performance.score")
	accessibility := tool.GetJsonFloat(outPutFile, "categories.accessibility.score")
	practices := tool.GetJsonFloat(outPutFile, "categories.best-practices.score")
	seo := tool.GetJsonFloat(outPutFile, "categories.seo.score")
	// // 第一个渲染完成时间
	// firstTime := tool.GetJsonFloatToStr(outPutFile, "audits.first-contentful-paint.numericValue")
	// // 最后一个渲染完成时间
	// lastTime := tool.GetJsonFloatToStr(outPutFile, "audits.largest-contentful-paint.numericValue")
	// // 总交互时间
	// interactiveTime := tool.GetJsonFloatToStr(outPutFile, "audits.interactive.numericValue")
	// // 总阻塞时间
	// blockTime := tool.GetJsonFloatToStr(outPutFile, "audits.total-blocking-time.numericValue")

	// 上报参数
	reportStruct.Performance = performance
	reportStruct.Accessibility = accessibility
	reportStruct.Practices = practices
	reportStruct.Seo = seo
	return nil
}

func (p *StructTask) GetDownlaodInfo(downloadUrl string, taskId string, reportStruct *ReportDownload) error {
	pathTmp := ".\\Download" + taskId
	err := os.Mkdir(pathTmp, 0666)
	if err != nil {
		log.Error().Msg(err.Error())
		return err
	}
	defer os.RemoveAll(pathTmp)

	outputFile := pathTmp + "\\downloadinfo.txt"
	execCmd := "wget " + downloadUrl + " -P " + pathTmp + " -o " + outputFile

	_, err = exec.Command("cmd", "/c", execCmd).Output()
	if err != nil {
		log.Error().Msg(err.Error())
		return err
	}
	time.Sleep(5 * time.Second)

	file, err := os.Open(outputFile)
	if err != nil {
		log.Error().Msg(err.Error())
		return err
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	//scanner.Split(bufio.ScanWords)
	scanner.Split(bufio.ScanLines)
	var downloadInfo []string
	for scanner.Scan() {
		downloadInfo = append(downloadInfo, scanner.Text())
	}
	downloadLen := len(downloadInfo)
	if downloadLen < 4 {
		err = errors.New("pingdownloadInfoInfo len error")
		log.Error().Msg(err.Error())
		return err
	}

	downloadInfoLine := downloadInfo[downloadLen-DOWNLOAD_LINE]
	// 带宽
	bwIndex1 := strings.Index(downloadInfoLine, `(`)
	bwIndex2 := bwIndex1
	for string(downloadInfoLine[bwIndex2]) != ")" {
		bwIndex2++
	}
	bandwidth := downloadInfoLine[bwIndex1+1 : bwIndex2]
	// 总大小
	sizeIndex1 := strings.Index(downloadInfoLine, `[`)
	sizeIndex2 := sizeIndex1
	for string(downloadInfoLine[sizeIndex2]) != "]" {
		sizeIndex2++
	}
	sizeStr := downloadInfoLine[sizeIndex1+1 : sizeIndex2]
	size := strings.Split(sizeStr, "/") // byte
	// 时间
	timeInfoLine := downloadInfo[downloadLen-DOWNLOAD_TIME_LINE]
	timeInfo := strings.Split(timeInfoLine, "=")
	// 上报参数
	reportStruct.Bandwidth = bandwidth
	reportStruct.Size = size[0]
	reportStruct.DownloadTime = timeInfo[len(timeInfo)-1]

	return err
}

func (p *StructTask) GetDNSInfo(dnsUrl string) (error, string, float64) {
	// dig baidu.com any 获取时间；不加any需要不加本地网关缓存
	execCmd := "dig " + dnsUrl
	dnsInfoStr, err := p.getExeCmdInfo(execCmd)
	if err != nil {
		log.Error().Msg(err.Error())
		return err, "", 0
	}
	dnsInfo := strings.Split(dnsInfoStr, "\n")
	// 防止dns服务器阻止，至少20s超时；
	//time.Sleep(20 * time.Second)
	// status
	if len(dnsInfo) < DNS_QUERY_STATUS_LINE {
		return errors.New("dns len err!"), "", 0
	}

	statusLine := dnsInfo[DNS_QUERY_STATUS_LINE]
	statusArr := strings.Split(statusLine, ":")
	if len(statusLine) < DNS_QUERY_STATUS {
		return errors.New("dns status len err!"), "", 0
	}
	status := strings.Split(statusArr[DNS_QUERY_STATUS], ",")
	statusStr := status[0]
	// dnstime
	queryTimeLine := dnsInfo[len(dnsInfo)-DNS_QUERY_TIME_LINE]
	queryTime := strings.Split(queryTimeLine, " ")
	dnstime, err := strconv.ParseFloat(queryTime[DNS_QUERY_TIME], 64)
	if err != nil {
		log.Error().Msg(err.Error())
	}
	return nil, statusStr, dnstime
}

func (p *StructTask) GetVideoTestInfo(videoUrl string, testTime string, reportStruct *ReportVideo) error {
	// videotest  -u http://10.29.48.162:8080/ -T 10
	execCmd := ".\\videotest -u " + videoUrl + " -T " + testTime
	outputStr, err := p.getExeCmdInfo(execCmd)
	if err != nil {
		return nil
	}
	bwIndex1 := strings.Index(outputStr, `{`)
	bwIndex2 := bwIndex1
	for string(outputStr[bwIndex2]) != "}" {
		bwIndex2++
	}
	videoTestStr := outputStr[bwIndex1 : bwIndex2+1]
	//2.声明空interface
	var i interface{}
	//3.解析
	err = json.Unmarshal([]byte(videoTestStr), &i)
	if err != nil {
		log.Error().Msg(err.Error())
	}
	//默认转成了map
	//4.解析到interface的json可以判断类型
	testMap := i.(map[string]interface{})
	block_cnt := testMap["block_cnt"].(float64)
	block_time := testMap["block_time"].(float64)   // 单位ms
	play_time := testMap["play_time"].(float64)     // 单位s
	connectTime := testMap["connectTime"].(float64) // 单位ms

	// 上报参数
	// 阻塞频率为每600s的阻塞次数
	reportStruct.BlockFreq = (block_cnt / play_time) * 600
	reportStruct.BlockTime = block_time
	reportStruct.ConnectTime = connectTime
	return err
}
