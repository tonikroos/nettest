package main

import (
	"flag"
	"nettest/client/connect"
	"nettest/client/update"
	rlog "nettest/log"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var (
	// 服务器地址
	serverAddr string
	LogPath    string
	LogLevel   int
	ver        bool
)

func init() {
	// ping
	flag.StringVar(&serverAddr, "serverAddr", ":8088", "ServerAddr")
	flag.StringVar(&LogPath, "log", "netclient.log", "defaut log path ")
	flag.IntVar(&LogLevel, "loglevel", 3, `(zerolog.PanicLevel, 5)
(zerolog.FatalLevel, 4)
(zerolog.ErrorLevel, 3)
(zerolog.WarnLevel, 2)
(zerolog.InfoLevel, 1)
(zerolog.DebugLevel, 0)
(zerolog.TraceLevel, -1)`)
	flag.BoolVar(&ver, "ver", false, "print version")
	flag.Parse()

}

func main() {
	rlog.SetPath(LogPath)
	rlog.SetLogLevel(LogLevel)
	if ver {
		update.PrtVersion()
		return
	}
	log.Info().Msgf("loglevel:%s", zerolog.GlobalLevel())
	go update.CheckUpdate()
	connect.ClientSocket(serverAddr)
}
