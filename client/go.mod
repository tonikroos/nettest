module nettest/client

go 1.15

require (
	github.com/axgle/mahonia v0.0.0-20180208002826-3358181d7394 // indirect
	github.com/caucy/batch_ping v0.0.0-20191010034743-107c95ad74d2
	github.com/dop251/goja v0.0.0-20210427212725-462d53687b0d // indirect
	github.com/rs/zerolog v1.21.0 // indirect
	github.com/thedevsaddam/gojsonq v2.3.0+incompatible // indirect
	github.com/thedevsaddam/gojsonq/v2 v2.5.2 // indirect
	golang.org/x/net v0.0.0-20201201195509-5d6afe98e0b7 // indirect
)
