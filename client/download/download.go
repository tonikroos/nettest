package download

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"
)

type Reader struct {
	io.Reader
	Total int64 //byte
	Cur   int64
}

func (r *Reader) Read(p []byte) (n int, err error) {
	n, err = r.Reader.Read(p)
	r.Cur += int64(n)
	// fmt.Printf("Cur:%.2f%%\n", float64((r.Cur*10000/r.Total)/100))
	return
}

func GetDownlaodInfo(url string, filename string) (Bandwidth string, Size string, DownloadTime string, err error) {
	rsp, err := http.Get(url)
	if err != nil {
		return
	}
	defer rsp.Body.Close()
	file, err := os.Create(filename)
	if err != nil {
		return
	}
	defer os.RemoveAll(filename)
	reader := &Reader{
		Reader: rsp.Body,
		Total:  rsp.ContentLength,
	}
	t1 := time.Now()
	_, err = io.Copy(file, reader)
	if err != nil {
		return
	}
	loadTime := float64(time.Since(t1) / 1000000) //ms
	size := reader.Total                          //byte
	bandwidth := float64(size) / loadTime         //kb/s
	// 转化为json字符串
	Bandwidth = strconv.FormatFloat(bandwidth, 'f', 2, 64)
	Size = strconv.FormatFloat(float64(size), 'f', 2, 64)
	DownloadTime = strconv.FormatFloat(loadTime, 'f', 2, 64)
	fmt.Printf("size:%v,loadTime:%v,bandwidth:%v\n", DownloadTime, Size, Bandwidth)
	return
}
